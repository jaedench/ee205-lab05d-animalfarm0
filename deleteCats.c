///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file deleteCats.c
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   15_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>

#include "catDatabase.h"
#include "deleteCats.h"

int deleteAllCats() {
   memset(names, 0, currentCats);
   memset(genders, 0, currentCats);
   memset(breeds, 0, currentCats);
   memset(isFixedd, 0, currentCats);
   memset(weights, 0, currentCats);
   currentCats = 0;
   printf("Deleted all cats\n");

   return 0;
}
