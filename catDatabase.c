///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file catDatabase.c
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   15_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "catDatabase.h"
#include <stdbool.h>

size_t currentCats = 0;

char names[MAX_CATS][MAX_LENGTH];

enum gender genders[MAX_CATS];

enum breed breeds[MAX_CATS];

bool isFixedd[MAX_CATS];

float weights[MAX_CATS];
  

