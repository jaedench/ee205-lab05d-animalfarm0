///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file catDatabase.h
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   15_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdbool.h>
#include <stdio.h>

#define MAX_CATS (30)
#define MAX_LENGTH (30)

extern size_t currentCats;

enum gender {UNKNOWN_GENDER, MALE, FEMALE};
enum breed {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};

extern char names[MAX_CATS][MAX_LENGTH];
extern enum gender genders[MAX_CATS];
extern enum breed breeds[MAX_CATS];
extern bool isFixedd[MAX_CATS];
extern float weights[MAX_CATS];

extern void initializeDatabase();
