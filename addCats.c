///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file addCats.c
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   15_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "addCats.h"
#include "catDatabase.h"
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

//#define DEBUG

int addCat(char name[], int gender, int breed, bool isFixed, float weight) {
   if (currentCats > MAX_CATS) {
      printf("Error: Too many cats!\n");
      return 1;
   }


   if (strlen(name) == 0) {
      printf("Error: No cat name detected!\n");
      return 1;
   }


   if (strlen(name) > MAX_LENGTH) {
      printf("Error: Cat name too long! Name must be 30 characters or less.\n");
      return 1;
   }

   
   for (int i = 0; i <= currentCats; ++i) {
      if (names[i] == name) {
         printf("Error: Name of cat must be unique!\n");
         return 1;
      }
   }


   if (weight < 0) {
      printf("Error: No cat detected! Weight must be greater than 0.\n");
      return 1;
   }

   strcpy(names[currentCats], name);
   genders[currentCats] = gender;
   breeds[currentCats] = breed;
   isFixedd[currentCats] = isFixed;
   weights[currentCats] = weight;

   currentCats += 1;
   printf("Cat successfully added!\n");
   return 0;

   #ifdef DEBUG
      printf("Current cats: %ld\n", currentCats);
      printf("Max length: %d\n", MAX_LENGTH);
      printf("Max cats: %d\n", MAX_CATS);
      printf("Name: %s\n", name);
      printf("Names of cats: %s\n", names[currentCats]);
      printf("Gender: %d\n", gender);
      printf("Genders of cats: %d\n", genders[currentCats]);
      printf("Breed: %d\n", breed);
      printf("Breeds of cats: %d\n", breeds[currentCats]);
      printf("Fixed: %d\n", isFixed);
      printf("Fixing of cats: %d\n", isFixedd[currentCats]);
      printf("Weight: %f\n", weight);
      printf("Weights of cats: %f\n", weights[currentCats]);
   #endif

}
