///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file updateCats.c
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   15_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "catDatabase.h"
#include "updateCats.h"

int updateCatName(int index, char newName[]) {
   if (index >= currentCats) {
      printf("Error: No cat at index [%d]\n", index);
      return 1;
   }


   if (index < 0) {
      printf("Error: Index must be 0 or larger\n");
      return 1;
   }


   if (strlen(newName) == 0) {
      printf("Error: No emptey name's allowed!\n");
      return 1;
   }


   if (strlen(newName) > MAX_LENGTH) {
      printf("Error: Cat's name is way too long! Must be 30 characters or less.\n");
      return 1;
   }


   for (int i = 0; i <= currentCats; ++i) {
      if (strcmp(newName, names[i]) == 0) {
         printf("Error: No duplicate names!\n");
         return 1;
      }
   }


   strcpy(names[index], newName);

   printf("Cat at index [%d] has been changed to [%s]!\n", index, newName);

   return 0;
}



int fixCat(int index) {
    if (index >= currentCats) {
      printf("Error: No cat at index [%d]\n", index);
      return 1;
   }


   if (index < 0) {
      printf("Error: Index must be 0 or larger\n");
      return 1;
   }

  
   if (isFixedd[index] == true) {
      printf("Error: Cat is already fixed! Too late to go back now...\n");
      return 1;
   }
   
   isFixedd[index] = true;
   printf("Cat at index [%d] is fixed now!\n", index);
   return 0;
}


int updateCatWeight(int index, float newWeight) {
   if (index >= currentCats) {
      printf("Error: No cat at index [%d]\n", index);
      return 1;
   }


   if (index < 0) {
      printf("Error: Index must be 0 or larger\n");
      return 1;
   }


   if (newWeight <= 0) {
      printf("Error: Is there a cat there? Weight must be greater than 0!\n");
      return 1;
   }


   weights[index] = newWeight;
   printf("Cat weight at index [%d] has been changed to [%f]\n", index, newWeight);
   return 0;
}

