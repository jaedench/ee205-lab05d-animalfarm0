///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file reportCats.c
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   15_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include "catDatabase.h"

int printCat(int index) {
   if (index < 0 || index >= currentCats) {
      printf("animalFarm0: Bad cat [%d]\n", index);
      return 1;
   }

   printf("cat index = [%d] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n", index, names[index], genders[index], breeds[index], isFixedd[index], weights[index]);

   return 0;
}


int printAllCats() {
   if (currentCats == 0) {
      printf("Error: No cats stored!\n");
      return 1;
   }
   

   //int i;
   for (int i = 0; i < currentCats; ++i) {
      printf("cat index =[%d] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n", i, names[i], genders[i], breeds[i], isFixedd[i], weights[i]);
   }
   return 0;
}

int findCat(char name[]) {
   int i = 0;
   if (strlen(name) == 0) {
      printf("Error: Hey! You need to enter a name!\n");
      return 1;
   }

   
   if (strlen(name) > MAX_LENGTH) {
      printf("Error: Name must be 30 characters or less! No cat's name is THAT long!\n");
      return 1;
   }

   for (i = 0; i < currentCats; ++i) {
      if (strcmp(name, names[i]) == 0) {
            printf("Match with cat at index [%d]\n", i);
            return i;
      }
   }      
      printf("Error: Cat not found!");
      return 1;
}

