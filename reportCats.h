///////////////////////////////////////////////////////////////////////////////
///        University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file reportCats.h
/// @version 1.0
///
/// @author Jaeden Chang <jaedench@hawaii.edu>
/// @date   15_Feb_2022
///////////////////////////////////////////////////////////////////////////////

int printCat(int index);

int printAllCats();

int findCat(char name[]);
