###############################################################################
#        University of Hawaii, College of Engineering
# @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
#
# @file Makefile
# @version 1.0
#
# @author Jaeden Chang <jaedench@hawaii.edu>
# @date   15_Feb_2022
###############################################################################

CC = gcc
CFLAGS = -g -Wall 

TARGET = animalFarm0

all: $(TARGET)

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

addCats.o: addCats.c addCats.h
	$(CC) $(CFLAGS) -c addCats.c

reportCats.o: reportCats.c reportCats.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h
	$(CC) $(CFLAGS) -c updateCats.c

deleteCats.o: deleteCats.c deleteCats.h
	$(CC) $(CFLAGS) -c deleteCats.c

main.o: main.c catDatabase.h addCats.h reportCats.h updateCats.h deleteCats.h
	$(CC) $(CFLAGS) -c main.c

animalFarm0: main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o

clean:
		rm -f $(TARGET) *.oi
